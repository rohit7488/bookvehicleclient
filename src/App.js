import { Steps, Step } from "react-step-builder";
import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";
import Step4 from "./Step4";
import Step5 from "./Step5";


function App() {
  return (
    <div className="App">
       <Steps>

        <Step component={Step1} />
        <Step component={Step2} />
        <Step component={Step3} />
        <Step component={Step4} />
        <Step component={Step5} />
      </Steps>
    </div>
  );
}

export default App;