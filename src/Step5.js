import axios from "axios"
import { useEffect, useState } from "react"

const Step5 = (props) => {

 const [allVehicles, setAllVehicles] = useState([]); 

    useEffect(() => {
        axios.get(`http://localhost:3000/vehicle/all`)
        .then((result) => {
            if(result.length < 1) {
                console.log('no vehicles at the moment.')
            }else {
               setAllVehicles(result.data);
            }
        })
        .catch();
     }, [])

    return(

         <div>
    <div className="bg-gray-200 h-screen grid place-items-center">
    <p className="lg:text-2xl text-lg text-gray-700">Our available vehicles</p>
    <div className="bg-gray-600 rounded-lg  flex justify-items-center flex-wrap content-center h-96 w-96">
    
    {
        console.log(allVehicles, 'all the vehicles')
        
    }



   {/* {
        allVehicles.map((vehicle, index) => {
            <p key={index}>{vehicle.modelName}</p>
        })
    }  */} 
    </div>
  
  <div>
  <button className="w-48 p-2 rounded-xl border-gray-800 border-2 hover:bg-black hover:text-white text-xl" onClick={props.prev}>Previous</button>&nbsp;&nbsp;
<button className="w-48 p-2 rounded-xl border-gray-800 border-2 hover:bg-black hover:text-white text-xl" onClick={props.next}>Next</button>
</div>
</div>
</div>
    )
}

export default Step5