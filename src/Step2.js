import React from "react";

function Step2(props) {
  return (
    <div>
        <div className="bg-gray-200 h-screen grid place-items-center">
        <p className="lg:text-2xl text-lg text-gray-700">Would you like to enjoy a Car Drive or a Bike Ride?</p>
        <div className="bg-gray-600 rounded-lg  flex justify-items-center flex-wrap content-center h-96 w-96">
        
     <p className="text-xl grid place-content-center pl-2 text-white w-48">2 Wheeler</p> <input value="twowheeler" name="wheels" type="radio" className=" h-4 w-4 mt-1"/>
     <p className="text-xl grid place-content-center pl-2 text-white w-48">4 Wheeler</p> <input value="fourwheeler" name="wheels" type="radio" className=" h-4 w-4 mt-1"/>
        </div>
      
      <div>
      <button className="w-48 p-2 rounded-xl border-gray-800 border-2 hover:bg-black hover:text-white text-xl" onClick={props.prev}>Previous</button>&nbsp;&nbsp;
<button className="w-48 p-2 rounded-xl border-gray-800 border-2 hover:bg-black hover:text-white text-xl" onClick={props.next}>Next</button>
</div>
    </div>
    </div>
  );
}

export default Step2;