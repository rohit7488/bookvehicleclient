import React from "react";

function Step1(props) { 
  return (
      <div>
    <div className="bg-gray-200 h-screen grid place-items-center">
        <p className="lg:text-2xl text-lg text-gray-700">Tell us your Full Name?</p>
        <div className="bg-gray-600 h-96 w-96 rounded-lg grid content-center">
        <p className="text-xl pl-2 text-white">First Name:&nbsp; <input style={{backgroundColor:'cyan'}} className="p-2 text-xl text-center text-gray-800 rounded-xl w-64" onChange={props.handleChange}  name="name" value={props.getState('name', '')}/></p>
      <p className="text-xl mt-4 pl-2 text-white">Last Name:&nbsp; <input className="p-2 text-xl text-gray-800 rounded-xl text-center w-64" onChange={props.handleChange}  name="surname" value={props.getState('surname', '')}/></p>
        </div>
      
      <div>
<button className="w-48 p-2 rounded-xl border-gray-800 border-2 hover:bg-black hover:text-white text-xl" onClick={props.next}>Next</button>
</div>
    </div>
    </div>
  );
}

export default Step1;